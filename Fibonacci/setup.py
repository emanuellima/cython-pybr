from distutils.core import setup
from Cython.Build import cythonize

arquivos = ["fib.pyx"]

setup(name='Fibonacci', ext_modules=cythonize(arquivos, annotate=True, language_level=3))
