from math import sin

def func(x):
    return sin(x**2)

def integrate_func(a, b, N):
    s = 0
    dx = (b - a)/N
    for i in range(N):
        s += func(a+i*dx)
    return s * dx

print(integrate_func(0, 1, 100000000))