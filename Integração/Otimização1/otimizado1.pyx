from math import sin

def func(double x):
    return sin(x**2)

def integrate_func(double a, double b, int N):
    cdef int i
    cdef double s, dx
    s = 0
    dx = (b - a)/N
    for i in range(N):
        s += func(a+i*dx)
    return s * dx
