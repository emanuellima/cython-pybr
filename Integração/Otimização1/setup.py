from distutils.core import setup
from Cython.Build import cythonize

arquivos = ["otimizado1.pyx"]

setup(
    name = "Otimizado",
    ext_modules = cythonize(arquivos, annotate=True, language_level=3)
) 
