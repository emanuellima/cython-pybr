from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize

ext_modules = [
    Extension("otimizado3",
              sources=["otimizado3.pyx"],
              libraries=["m"]  # Liga a biblioteca matemática
              )
]

setup(
    name = "Otimizado",
    ext_modules = cythonize(ext_modules, annotate=True, language_level=3)
) 
